
# TEST AUTOMATION MODEL: LeadSpark

====================

  

## Requirements

  

- [Node.Js](https://nodejs.org/en/) version 14 or higher

  

## Quick start    :rocket:

  

1. Download the latest stable release [here](https://bitbucket.org/drivek/leadspark-test-automation-platform/src) or clone the git repo — `git clone git@bitbucket.org:drivek/leadspark-test-automation-platform.git`

  

2. Install the dependencies (`npm ci`)

  

Now you are ready to run and write your own features!  :clap:

  

## Features  :package:

  

- Super simple setup

- Full integration with [WebdriverIO](http://webdriver.io/)

-  **true**  [BDD](http://en.wikipedia.org/wiki/Behavior-driven_development). Instead of writing complicated test code that only developers can understand, Cucumber maps an ordinary language to code and allows to start with the test process in the early stages of your product development.

- Over 150 predefined steps that cover almost everything you need, you can start writing tests right away

- Easy integration with cloud services like [Browser Stack](https://www.browserstack.com/)

  

# How to write a test  :pencil:

  

Tests are written in [Gherkin syntax](https://cucumber.io/docs/gherkin/)

that means that you write down what's supposed to happen in a real language. All test files are located in

`./src/features/featureFiles/*` and have the file ending `.feature`. You will already find some test files in that

directory. They should demonstrate, how tests could look like. Just create a new file and write your first

test.

  

__myFirstTest.feature__

```gherkin

Feature:

In order to keep my product stable

As a developer or product manager

I want to make sure that everything works as expected

  

Scenario: Check title of website after search

Given I open the url "http://google.com"

When I set "WebdriverIO" to the inputfield "#lst-ib"

And I press "Enter"

Then I expect that the title is "WebdriverIO - Google Search"

  

Scenario: Another test

Given ...

  

```

  

This test opens the browser and navigates them to google.com to check if the title contains the search

query after doing a search. As you can see, it is pretty simple and understandable for everyone.

  

# How to run tests  :mag_right:
  

Build the code:

  

```sh

$ npm run build

```

  

Run the tests:

  

```sh

$ npm run runAll

```

  

Sometimes it's useful to only execute a single feature file, to do so use the following command:

  

```sh

$ npx wdio wdio.conf.js --spec ./src/features/featureFiles/select.feature

```

  

# Using tags

  

If you want to run only specific tests you can mark your features with tags. These tags will be placed before each feature like so:

  

```gherkin

@Tag

Feature: ...

```

  

To run only the tests with specific tag(s) use the `--cucumberOpts.tagExpression=` parameter like so:

  

```sh

$ npx wdio wdio.conf.js --cucumberOpts.tagExpression='@Tag or @AnotherTag'

```

  

For more tag options please see the [Cucumber.js documentation](https://docs.cucumber.io/tag-expressions/)

  

# Pending test

  

If you have failing or unimplemented tests you can mark them as "Pending" so they will get skipped.

  

```gherkin

// skip whole feature file

@Pending

Feature: ...

  

// only skip a single scenario

@Pending

Scenario: ...

```