const browsers = {
  chromeDesktop: {
    maxInstances: 6,
    browserName: 'chrome',
    platformName: 'ANY',
  },
  firefox: {
    maxInstances: 6,
    browserName: 'firefox',
    platformName: 'ANY',
  },
  chromeLocal: {
    maxInstances: 1,
    browserName: 'chrome',
  },
};

export default browsers;
