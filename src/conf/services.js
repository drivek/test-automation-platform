const services = {
  bsLocal: {
    user: 'motork1',
    key: 'CpvcFxy41mqTUWXXTesB',
    services: [
      ['browserstack', {
        browserstackLocal: true,
        forcedStop: true,
      }],
    ],
    host: 'hub.browserstack.com',
    /* eslint-disable */
    // Code to start browserstack local before start of test
    onPrepare(config, capabilities) {
      console.log('Connecting local');
      return new Promise(((resolve, reject) => {
        exports.bs_local = new browserstack.Local();
        exports.bs_local.start({ key: this.key }, (error) => {
          if (error) return reject(error);

          console.log('Connected. Now testing...');
          resolve();
        });
      }));
    },

    // Code to stop browserstack local after end of test
    onComplete(exitCode, config, capabilities, results) {
      exports.bs_local.stop();
    },
    /* eslint-enable */
  },
  grid: {
    protocol: 'http',
    hostname: 'qa-sel-hub.docker.motork.io',
    port: 4444,
    path: '/wd/hub',
  },
  local: {
    runner: 'local',
    port: 4723,
    baseUrl: 'http://localhost',
    services: [
      ['chromedriver', {
        outputDir: 'driver-logs',
        args: ['--headless --silent --disable-dev-shm-usage --disable-extensions --disable-gpu --no-sandbox'],
      }]],
  },
};
export default services;
