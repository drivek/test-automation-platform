/* eslint-disable import/no-extraneous-dependencies */
import services from './services';
import mobiles from './devices/mobile';
import desktop from './devices/desktop';

const { argv } = require('yargs/yargs');
const wdioParallel = require('wdio-cucumber-parallel-execution');

const sourceSpecDirectory = './src/features/featureFiles';

const manageBrowserCaps = (isChrome) => {
  const capabilities = [];
  if (isChrome) {
    capabilities.push(desktop.chromeDesktop);
  } else {
    capabilities.push(desktop.firefox);
  }
  return Object.assign(services.grid, { capabilities });
};

const manageMultiBrowser = () => {
  const capabilities = [];
  capabilities.push(desktop.chromeDesktop);
  capabilities.push(desktop.firefox);
  return Object.assign(services.grid, { capabilities });
};

const manageMobileCaps = (isIos) => {
  const capabilities = [];
  if (isIos) {
    capabilities.push(mobiles.iPhone.default);
  } else {
    capabilities.push(mobiles.android.default);
  }
  return Object.assign(services.bsLocal, { capabilities });
};

const setCaps = (target) => {
  const capabilities = [];
  switch (target) {
    case 'Mobile: iOS':
      return manageMobileCaps(true);
    case 'Mobile: Android':
      return manageMobileCaps(false);
    case 'Chrome Only':
      return manageBrowserCaps(true);
    case 'Firefox':
      return manageBrowserCaps(false);
    case 'Chrome & Firefox':
      return manageMultiBrowser();
    default:
      capabilities.push(desktop.chromeLocal);
      // console.log('CAPABILITIES: ', capabilities);
      // console.log('BASE_URL: ', process.env.BASE_URL);
      return Object.assign(services.local, { capabilities });
  }
};

const setSpecs = () => {
  // If parallel execution is set to true, then create the Split the feature files
  // And store then in a tmp spec directory (created inside `the source spec directory)
  let featureFilePath = `${sourceSpecDirectory}/*.feature`;
  if (process.env.TARGET === 'local') {
    argv.parallel = 'false';
  } else {
    argv.parallel = 'true';
    const tmpSpecDirectory = `${sourceSpecDirectory}/tmp`;
    wdioParallel.performSetup({
      sourceSpecDirectory,
      tmpSpecDirectory,
      cleanTmpSpecDirectory: true,
    });
    featureFilePath = `${tmpSpecDirectory}/*.feature`;
  }
  return featureFilePath;
};

export { setCaps, setSpecs };
