Feature: Homepage

This feature file contains all scenarios concerning the widgets in HP.

  @test
  Scenario: Testing fetch json data
  
    Given I open the url "home"
    * I have a screen that is 1920 by 1080 pixels
    * I expect the url to contain "keycloak"
    When I add "usrIt" to the inputfield "usernameField"
    * I add "pwdIt" to the inputfield "passwordField"
    * I click on the button "submitButton"
    Then I expect that the url is "home"