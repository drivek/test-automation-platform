import getDataInput from '../../../dataInput';

/**
 * Open the given URL
 * @param  {String}   type Type of navigation (getUrl or site)
 * @param  {String}   page The URL to navigate to
 */
export default (type, page) => {
  /**
     * The URL to navigate to
     * @type {String}
     */
  const thePage = getDataInput(page);
  const url = (type === 'url') ? thePage : browser.options.baseUrl + thePage;
  browser.url(url);
};
