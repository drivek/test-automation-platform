import getDataInput from '../../../dataInput';
/**
 * Scroll the page to the given element
 * @param  {String}   selector Element selector
 */
export default (selector) => {
  const theSelector = getDataInput(selector);
  $(theSelector).scrollIntoView();
};
