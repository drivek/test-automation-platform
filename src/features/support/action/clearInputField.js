import getDataInput from '../../../dataInput';
/**
 * Clear a given input field (placeholder for WDIO's clearElement)
 * @param  {String}   selector Element selector
 */
export default (selector) => {
  const theSelector = getDataInput(selector);
  $(theSelector).clearValue();
};
