/**
 * Set an attribute to a value in the given element
 * @param  {String}   attribute The attribute you want to set
 * @param  {String}   value     The value
 * @param  {String}   id        Element id
 */
export default (attribute, value, id) => {
  try {
    browser.execute((el, attr, val) => {
      // eslint-disable-next-line no-undef
      document.querySelector(el).setAttribute(attr, val);
    }, id, attribute, value);
  } catch (error) {
    expect(error).toBe('', `The element ${id} did not change attribute.`);
  }
};
