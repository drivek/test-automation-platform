import checkIfElementExists from '../lib/checkIfElementExists';
import getDataInput from '../../../dataInput';
/**
 * Set the value of the given input field to a new value or add a value to the
 * current selector value
 * @param  {String}   method  The method to use (add or set)
 * @param  {String}   value   The value to set the selector to
 * @param  {String}   selector Element selector
 */
export default (method, value, selector) => {
  const theSelector = getDataInput(selector);
  const theValue = getDataInput(value)
  /**
     * The command to perform on the browser object (addValue or setValue)
     * @type {String}
     */
  const command = (method === 'add') ? 'addValue' : 'setValue';

  let checkValue = theValue;

  checkIfElementExists(theSelector, false, 1);

  if (!value) {
    checkValue = '';
  }

  $(theSelector)[command](checkValue);
};
