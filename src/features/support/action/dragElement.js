import getDataInput from '../../../dataInput';
/**
 * Drag a element to a given destination
 * @param  {String}   selector      The selector for the source element
 * @param  {String}   destination The selector for the destination element
 */

export default (selector, destination, X, Y) => {
  const theSelector = getDataInput(selector);
  if (destination) {
    const theDestination = getDataInput(destination);
    $(theSelector).dragAndDrop($(theDestination));
  } else {
    $(theSelector).dragAndDrop({ x: X, y: Y });
  }
};
