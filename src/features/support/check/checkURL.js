import getDataInput from '../../../dataInput';
/**
 * Check the URL of the given browser window
 * @param  {String}   falseCase   Whether to check if the URL matches the
 *                                expected value or not
 * @param  {String}   expectedUrl The expected URL to check against
 */
export default (falseCase, expectedUrl) => {
  const theExpectedUrl = getDataInput(expectedUrl);
  /**
     * The current browser window's URL
     * @type {String}
     */
  const currentUrl = browser.getUrl();

  if (falseCase) {
    expect(currentUrl)
      .not.toEqual(theExpectedUrl, `expected url not to be "${currentUrl}"`);
  } else {
    expect(currentUrl).toEqual(
      theExpectedUrl,
      `expected url to be "${theExpectedUrl}" but found `
            + `"${currentUrl}"`,
    );
  }
};
