import getDataInput from '../../../dataInput';
/**
 * Check if the given element exists in the DOM one or more times
 * @param  {String}  selector  Element selector
 */
export default (selector) => {
  const theSelector = getDataInput(selector);
  try {
    // eslint-disable-next-line no-undef
    browser.execute((el) => { document.querySelector(el).click(); }, theSelector);
  } catch (error) {
    expect(false).toBe(true, `The element ${selector} is not clickable.`);
  }
};
