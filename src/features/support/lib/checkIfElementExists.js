import getDataInput from '../../../dataInput';
/**
 * Check if the given element exists in the DOM one or more times
 * @param  {String}  selector  Element selector
 * @param  {Boolean} falseCase Check if the element (does not) exists
 * @param  {Number}  exactly   Check if the element exists exactly this number
 *                             of times
 */
export default (selector, falseCase, exactly) => {
  /**
     * The number of elements found in the DOM
     * @type {Int}
     */
  const theSelector = getDataInput(selector);
  const nrOfElements = $$(theSelector);

  if (falseCase === true) {
    expect(nrOfElements).toHaveLength(
      0,
      `Element with selector "${theSelector}" should not exist on the page`,
    );
  } else if (exactly) {
    expect(nrOfElements).toHaveLength(
      parseInt(exactly, 10),
      `Element with selector "${theSelector}" should exist exactly `
            + `${exactly} time(s)`,
    );
  } else {
    expect(nrOfElements.length).toBeGreaterThanOrEqual(
      1,
      `Element with selector "${theSelector}" should exist on the page`,
    );
  }
};
