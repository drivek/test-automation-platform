/**
 * smoke test on production sites' sitemap
 * @param  {String} url Websites production url
 */
export default (url) => {
  browser.url(`${url}sitemap_index.xml`);
  browser.pause(3000);
  const map = $$('//*[@id="sitemap"]//a');
  const mapFound = [];
  map.forEach((link) => {
    const theLink = link.getAttribute('href');
    if (!theLink.includes(url)) {
      mapFound.push(link.getText());
    }
  });
  expect(mapFound.toString())
    .toEqual('');
};
