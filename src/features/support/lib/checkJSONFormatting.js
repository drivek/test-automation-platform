/**
 * Check if the given text is a correctly formatted json
 * @param  {String}  selector  Element selector
 */
export default (selector) => {
  const str = $(selector).getText();
  try {
    JSON.parse(str);
  } catch (e) {
    expect(false).toBe(true);
  }
};
