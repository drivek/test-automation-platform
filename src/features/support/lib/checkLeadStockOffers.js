export default (selector) => {
  const keys = ['demo-it.wsr74.dealerk', 'test@dealerk.com',
    'stockOffer', 'offerAmount', 'discountType'];
  const keysNotFound = [];
  const smtg = $(selector).getText();
  keys.forEach((key) => {
    if (!smtg.includes(key)) {
      keysNotFound.push(key);
    }
  });
  expect(keysNotFound.toString())
    .toEqual('');
};
