import clickJs from './clickJs';
import getDataInput from '../../../dataInput';
/**
 * Check if the a banner is present on the page for cookies
 * @param  {String}  selector  Element selector
 */
export default (selector) => {
  const theSelector = getDataInput(selector);
  /**
     * The number of elements found in the DOM
     * @type {Int}
     */
  try {
    const nrOfElements = $$(theSelector);

    if (nrOfElements !== 0) {
      clickJs(theSelector);
    }
  } catch (e) {
    expect(true).toBe(true);
  }
};
