/**
 * Fetch data from jsons
 * @param  {String}  key  data you want to fetch
 */
import homePage from '../../../dataInput/homePage';
import stockListing from '../../../dataInput/stockListing';

export default (key) => {
  const data = Object.assign(homePage, stockListing);
  return data.String(key);
};
