import checkJSONFormatting from './checkJSONFormatting';
import checkLeadLoan from './checkLeadLoan';
import checkLeadStockOffers from './checkLeadStockOffers';

/**
 * Check if the given text is a correctly formatted json
 * @param  {String}  selector  Element selector
 * @param  {String}  formType  type of form you want to check the lead for (Stock offers, loans etc)
 */
export default (selector, formType) => {
  checkJSONFormatting(selector);
  switch (formType) {
    case 'loan':
      checkLeadLoan(selector);
      break;
    case 'stock offers':
      checkLeadStockOffers(selector);
      break;

    default:
      break;
  }
};
