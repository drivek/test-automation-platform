/* eslint-disable import/no-extraneous-dependencies */
import { Then } from '@cucumber/cucumber';
import sitemapSmoke from '../support/lib/sitemapSmoke';
import checkJSONFormatting from '../support/lib/checkJSONFormatting';
import checkLeadByForm from '../support/lib/checkLeadByForm';
import checkPropertyContent from '../support/lib/checkPropertyContent';

Then(/^I check the sitemap "([^"]*)?" for broken pages$/,
  sitemapSmoke);

Then(/^I expect that "([^"]*)?" has a correctly formatted json$/,
  checkJSONFormatting);

Then(/^I check lead in the selector "([^"]*)?" for values from widget "([^"]*)?"$/,
  checkLeadByForm);

Then(/^I expect that the( css)* attribute "([^"]*)?" from element "([^"]*)?" does( not)* contain "([^"]*)?"$/,
  checkPropertyContent);
