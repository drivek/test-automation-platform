/* eslint-disable import/no-extraneous-dependencies */
import { When } from '@cucumber/cucumber';
import clickJs from '../support/lib/clickJs';
import setAttribute from '../support/action/setAttribute';

When(
  /^I click via JS the element "([^"]*)?"$/,
  clickJs,
);

When(
  /^I set the attribute "([^"]*)?" to value "([^"]*)?" in element "([^"]*)?"$/,
  setAttribute,
);