/* eslint-disable import/no-extraneous-dependencies */
import { Given } from '@cucumber/cucumber';
import checkCookieBanner from '../support/lib/checkCookieBanner';

Given(
  /^I dismiss the cookie banner button "([^"]*)?" if present$/,
  checkCookieBanner,
);
