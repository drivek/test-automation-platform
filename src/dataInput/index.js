/**
 * Fetch data from jsons
 * @param  {String}  input  data you want to fetch
 */
import creds from './credentials';
import loginPage from './loginPage';
import urls from './urls';

const data = {
  ...urls,
  ...creds,
  ...loginPage,
};

export default (input) => (input in data ? data[input] : input);
