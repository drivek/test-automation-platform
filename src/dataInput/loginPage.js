const loginPage = {
    usernameField: "#username",
    passwordField: "#password",
    submitButton: "#kc-login",
};

export default loginPage;