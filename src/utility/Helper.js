class Helper {
  getRandomInt(min, max) {
    return Math.floor(Math.random() * Math.floor(max - min) + Math.floor(min));
  }
}

export default new Helper();
