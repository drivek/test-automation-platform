/**
 * In this library, implement custom commands to accomodate "undefined" error.
 */
const getAndCheckUrl = (urlToCheck) => {
  expect(urlToCheck).toEqual(browser.getUrl(), `The expected url ${urlToCheck} is not correct.`);
};

const waitForElement = (sel) => {
  try {
    sel.waitForDisplayed({ timeout: 2000 });
  } catch (error) {
    expect(false).toBe(true, `The element ${sel} is not visible.`);
  }
};

const jsClick = (selector) => {
  try {
    // eslint-disable-next-line no-undef
    browser.execute((el) => { document.querySelector(el).click(); }, selector);
  } catch (error) {
    expect(false).toBe(true, `The element ${selector} is not clickable.`);
  }
};

const waitAndClick = (sel) => {
  try {
    waitForElement(sel);
    sel.scrollIntoView({ block: 'center' });
    sel.click();
  } catch (error) {
    expect(false).toBe(true, `The element ${sel} is not clickable.`);
  }
};

module.exports = {
  getAndCheckUrl, waitForElement, waitAndClick, jsClick,
};
